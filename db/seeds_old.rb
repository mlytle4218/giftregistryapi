User.create!([
  {name: "Barbara", email: "barbara.smith@example.com", password: "111111111", last: "Smith", phone: "1234561111"},
  {name: "Charles", email: "charles.johnson@example.com", password: "112112112", last: "Johnson", phone: "1234562222"},
  {name: "David", email: "david.williams@example.com", password: "113113113", last: "Williams", phone: "1234563333"},
  {name: "Dorothy", email: "dorothy.brown@example.com", password: "114114114", last: "Brown", phone: "1234564444"},
  {name: "Joshua", email: "joshua.reilly@example.com", password: "115115115", last: "Reilly", phone: "1234565555"},
  {name: "Guinevere", email: "guinevere.small@example.com", password: "116116116", last: "Small", phone: "1234566666"},
  {name: "Dieter", email: "dieter.le@example.com", password: "117117117", last: "Le", phone: "1234567777"},
  {name: "Rebecca", email: "rebecca.daugherty@example.com", password: "118118118", last: "Daugherty", phone: "1234568888"},
  {name: "Kennan", email: "kennan.barron@example.com", password: "119119119", last: "Barron", phone: "1234569999"}
])
Product.create!([
  {name: "Cheetos Jumbo Puffs, 8.5 ounce", upc: "028400239875", description: "Description of product unavailable at this time.", image: "http://ecx.images-amazon.com/images/I/51ExH0S5nsL._SL160_.jpg"},
  {name: "Beats By Dr. Dre Studio White Headphones", upc: "848447001156", description: "Description of product unavailable at this time.", image: "http://images.prosperentcdn.com/images/250x250/content.abt.com/media/images/products/l1_MH7E2AMA.jpg"},
  {name: "Garmin VIRB XE action camera with G-Metrix 1080p/60fps, 240 fps HD slow motion a", upc: "753759131814", description: "Description of product unavailable at this time.", image: "http://www.adorama.com/images/large/GAVXEACWGM.JPG"},
  {name: "SanDisk 32GB USB 32G Cruzer Orbit SDCZ58 Flash Pen Thumb Drive USB 2.0 with Lany", upc: "619659090500", description: "Description of product unavailable at this time.", image: "http://img1.r10.io/PIC/56244634/0/1/250/56244634.jpg"},
  {name: "Dockers Misses - Trapunto Stitch Cargo Shorts (Fuchsia Purple) Women's Shorts", upc: "736401815737", description: "Description of product unavailable at this time.", image: "http://www.6pm.com/images/z/3/2/3/8/7/4/3238749-p-2x.jpg"},
  {name: "Netgear High Speed DOCSIS 3.0 Cable Modem (CM400-1AZNAS)", upc: "606449111224", description: "Description of product unavailable at this time.", image: "http://ecx.images-amazon.com/images/I/410tb4q6XQL._SL160_.jpg"},
  {name: "PURELL - Advanced Instant Hand Sanitizer with Aloe, 8 oz Bottle - 24/Carton", upc: "073852062380", description: "Description of product unavailable at this time.", image: "http://scene7.samsclub.com/is/image/samsclub/0007385206238_A?$img_size_211x208$"},
  {name: "New and Essential Spiralizer Tri-Blade Spiral Vegetable Slicer", upc: "848441000087", description: "Description of product unavailable at this time.", image: "/static/img/resize.jpg"},
  {name: "Etekcity® High Precision (0.2lb) 400lb /180kg Digital Bathroom Scale, \"Smart St", upc: "025706343039", description: "Description of product unavailable at this time.", image: "http://ecx.images-amazon.com/images/I/31ydpwo2jpL._SL160_.jpg"}
])
List.create!([
  {name: "Default", user_id: 1},
  {name: "Default", user_id: 2},
  {name: "Default", user_id: 3},
  {name: "Default", user_id: 4},
  {name: "Default", user_id: 5},
  {name: "Default", user_id: 6},
  {name: "Default", user_id: 7},
  {name: "Default", user_id: 8},
  {name: "Default", user_id: 9},
  {name: "Barbara and Charles", user_id: 1},
  {name: "David and Dorothy", user_id: 3}
])
Listitem.create!([
  {number: 1, product_id: 1, list_id: 1},
  {number: 1, product_id: 2, list_id: 2},
  {number: 2, product_id: 3, list_id: 3},
  {number: 1, product_id: 4, list_id: 4},
  {number: 1, product_id: 5, list_id: 5},
  {number: 3, product_id: 6, list_id: 6},
  {number: 1, product_id: 7, list_id: 7},
  {number: 1, product_id: 8, list_id: 8},
  {number: 4, product_id: 9, list_id: 9},
  {number: 1, product_id: 1, list_id: 10},
  {number: 1, product_id: 2, list_id: 11},
  {number: 1, product_id: 3, list_id: 1},
  {number: 1, product_id: 6, list_id: 1}
])
Listmember.create!([
  {user_id: 2, list_id: 10},
  {user_id: 4, list_id: 11}
])


