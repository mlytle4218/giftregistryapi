
Product.create!([
  {name: "Cheetos Jumbo Puffs, 8.5 ounce", upc: "028400239875", description: "Cheetos Puffs Cheese Flavored Snacks: Made with real cheese Gluten free", image: "http://ecx.images-amazon.com/images/I/51ExH0S5nsL._SL160_.jpg", ean: "0028400239875", brand: "Cheetos", model: "0002840023987", dimension: "", weight: ""},
  {name: "Beats By Dr. Dre Studio White Headphones", upc: "848447001156", description: "Completely Redesigned, The Beats Studio Over-Ear Headphones Are Even Lighter, Stronger, And More Comfortable, And More Precise Than The Original World-Famous Design/ Beats Acoustic Engine Gives You An Intimate, Real Encounter With Your Music/ No-Compromi", image: "http://www.adorama.com/images/large/BTOVSTUWHT.JPG", ean: "0848447001156", brand: "Beats by Dr. Dre", model: "848447001156", dimension: "3.1 X 6.8 X 7.4 inches", weight: "3.6 Pounds"},
  {name: "Garmin VIRB XE action camera with G-Metrix 1080p/60fps, 240 fps HD slow motion a", upc: "753759131814", description: "G-METRIX-Built-in GPS and external sensors with connectivity to Garmin devices to capture performance data like speed, elevation, heart rate and G-force.ADVANCED VIDEO - Capture thrilling footage with high definition 1080p resolution at 60 frames-per-sec", image: "http://images.prosperentcdn.com/images/250x250/ih.atensoftware.com/84468/garmin-virb-xe-hd-action-camera-garcam0180-15.jpg", ean: "0753759131814", brand: "Garmin", model: "010-01363-11", dimension: "", weight: ""},
  {name: "SanDisk 32GB USB 32G Cruzer Orbit SDCZ58 Flash Pen Thumb Drive USB 2.0 with Lany", upc: "619659090500", description: "SanDisk Retail Package", image: "http://img1.r10.io/PIC/56244634/0/1/250/56244634.jpg", ean: "0619659090500", brand: "SanDisk", model: "SDCZ58-32G-L", dimension: "", weight: ""},
  {name: "Dockers Misses - Trapunto Stitch Cargo Shorts (Fuchsia Purple) Women's Shorts", upc: "736401815737", description: "Flat-front cargo short with slanted side pockets.Zip fly with button.Welted back pockets.", image: "http://s7d2.scene7.com/is/image/SpecialtyRetailers/MI-226739-000000-B?$zm$", ean: "0736401815737", brand: "Dockers women's by Hampshire", model: "05W50015M", dimension: "", weight: ""},
  {name: "Netgear High Speed DOCSIS 3.0 Cable Modem (CM400-1AZNAS)", upc: "606449111224", description: "Description of product unavailable at this time.", image: "http://ecx.images-amazon.com/images/I/410tb4q6XQL._SL160_.jpg", ean: "0606449111224", brand: "Netgear", model: "CM400-1AZNAS", dimension: "", weight: ""},
  {name: "PURELL - Advanced Instant Hand Sanitizer with Aloe, 8 oz Bottle - 24/Carton", upc: "073852062380", description: "Description of product unavailable at this time.", image: "http://content.oppictures.com/Master_Images/Master_Variants/Variant_100/238299.JPG", ean: "0073852062380", brand: "Purell", model: "GOJ967406ECDECO", dimension: "", weight: ""},
  {name: "New and Essential Spiralizer Tri-Blade Spiral Vegetable Slicer", upc: "848441000087", description: "Description of product unavailable at this time.", image: "", ean: "0848441000087", brand: "Paderno World Cuisine", model: "48297-99", dimension: "23 X 15 X 1 inches", weight: "3 pounds"},
  {name: "Etekcity® High Precision (0.2lb) 400lb /180kg Digital Bathroom Scale, \"Smart St", upc: "025706343039", description: "\"Step-and-Read\" - Immediate readings as soon as you step on the bathroom scale; Measurement Range:11lb-400lb (5kg-180kg).High accuracy - 4 latest-version high-precision sensors, using technology from Germany.Multifunction - Auto-power-off, Auto-zero, Low", image: "http://images.hookbag.ca/3a46da71962bb948/image.jpg", ean: "0025706343039", brand: "Etekcity", model: "025706343039", dimension: "", weight: ""}
])
User.create!([
  {name: "Barbara", email: "barbara.smith@example.com", password: "111111111", last: "Smith", phone: "1234561111"},
  {name: "Charles", email: "charles.johnson@example.com", password: "112112112", last: "Johnson", phone: "1234562222"},
  {name: "David", email: "david.williams@example.com", password: "113113113", last: "Williams", phone: "1234563333"},
  {name: "Dorothy", email: "dorothy.brown@example.com", password: "114114114", last: "Brown", phone: "1234564444"},
  {name: "Joshua", email: "joshua.reilly@example.com", password: "115115115", last: "Reilly", phone: "1234565555"},
  {name: "Guinevere", email: "guinevere.small@example.com", password: "116116116", last: "Small", phone: "1234566666"},
  {name: "Dieter", email: "dieter.le@example.com", password: "117117117", last: "Le", phone: "1234567777"},
  {name: "Rebecca", email: "rebecca.daugherty@example.com", password: "118118118", last: "Daugherty", phone: "1234568888"},
  {name: "Kennan", email: "kennan.barron@example.com", password: "119119119", last: "Barron", phone: "1234569999"}
])
List.create!([
  {name: "Barbara's Default", user_id: 1},
  {name: "Charles's Default", user_id: 2},
  {name: "David's Default", user_id: 3},
  {name: "Dorothy's Default", user_id: 4},
  {name: "Joshua's Default", user_id: 5},
  {name: "Guinevere's Default", user_id: 6},
  {name: "Dieter's Default", user_id: 7},
  {name: "Rebecca's Default", user_id: 8},
  {name: "Kennan's Default", user_id: 9},
  {name: "Barbara and Charles", user_id: 1},
  {name: "David and Dorothy", user_id: 3}
])
Listitem.create!([
  {number: 1, product_id: 1, list_id: 1},
  {number: 1, product_id: 2, list_id: 2},
  {number: 2, product_id: 3, list_id: 3},
  {number: 1, product_id: 4, list_id: 4},
  {number: 1, product_id: 5, list_id: 5},
  {number: 3, product_id: 6, list_id: 6},
  {number: 1, product_id: 7, list_id: 7},
  {number: 1, product_id: 8, list_id: 8},
  {number: 4, product_id: 9, list_id: 9},
  {number: 1, product_id: 1, list_id: 10},
  {number: 1, product_id: 2, list_id: 11},
  {number: 1, product_id: 3, list_id: 1},
  {number: 1, product_id: 6, list_id: 1}
])
Listmember.create!([
  {user_id: 2, list_id: 10},
  {user_id: 4, list_id: 11}
])
Following.create!([
  {users_id: 1, lists_id: 4}
])
