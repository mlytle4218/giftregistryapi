class CreateListitems < ActiveRecord::Migration
  def change
    create_table :listitems do |t|
      t.integer :number
      t.references :product, index: true, foreign_key: true
      t.references :list, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
