class CreateFollowings < ActiveRecord::Migration
  def change
    create_table :followings do |t|
      t.references :users
      t.references :lists

      t.timestamps null: false
    end
  end
end
