# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create(
    [
      {name: 'Barbara', last:"Smith", email: 'barbara.smith@example.com', password: '111111111', phone: '1234561111'},
      {name: 'Charles', last:"Johnson", email: 'charles.johnson@example.com', password: '112112112', phone: '1234562222'},
      {name: 'David', last:"Williams", email: 'david.williams@example.com', password: '113113113', phone: '1234563333'},
      {name: 'Dorothy', last:"Brown", email: 'dorothy.brown@example.com', password: '114114114', phone: '1234564444'},
      {name: 'Joshua', last: 'Reilly', email: 'joshua.reilly@example.com', password: '115115115', phone: '1234565555'},
      {name: 'Guinevere', last: 'Small', email: 'guinevere.small@example.com', password: '116116116', phone: '1234566666'},
      {name: 'Dieter', last: 'Le', email: 'dieter.le@example.com', password: '117117117', phone: '1234567777'},
      {name: 'Rebecca', last: 'Daugherty', email: 'rebecca.daugherty@example.com', password: '118118118', phone: '1234568888'},
      {name: 'Kennan', last: 'Barron', email: 'kennan.barron@example.com', password: '119119119', phone: '1234569999'},      
    ]
  )
  
  
  
product = Product.create(
  [
    {name: 'Product1', upc: '1111', description: 'product 1 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product2', upc: '2222', description: 'product 2 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product3', upc: '3333', description: 'product 3 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product4', upc: '4444', description: 'product 4 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product5', upc: '5555', description: 'product 5 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product6', upc: '6666', description: 'product 6 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'},
    {name: 'Product7', upc: '7777', description: 'product 7 description', image: 'http://www.lamar.edu/cel/_files/images/photo_not_available.png'}
  ]
)
list = List.create(
  [
    {user_id: '1', name: 'Default'},
    {user_id: '2', name: 'Default'},
    {user_id: '3', name: 'Default'},
    {user_id: '4', name: 'Default'},
    {user_id: '5', name: 'Default'},
    {user_id: '6', name: 'Default'},
    {user_id: '7', name: 'Default'},
    {user_id: '8', name: 'Default'},
    {user_id: '9', name: 'Default'},
    {user_id: '1', name: 'Barbara and Charles'},
    {user_id: '3', name: 'David and Dorothy'},
  ]
)
listitem = Listitem.create(
  [
    {list_id: '1', product_id: '1', number: '1'},
    {list_id: '2', product_id: '2', number: '1'},
    {list_id: '3', product_id: '3', number: '2'},
    {list_id: '4', product_id: '4', number: '1'},
    {list_id: '5', product_id: '5', number: '1'},
    {list_id: '6', product_id: '6', number: '3'},
    {list_id: '7', product_id: '7', number: '1'},
    {list_id: '8', product_id: '1', number: '1'},
    {list_id: '9', product_id: '2', number: '4'},
    {list_id: '10', product_id: '3', number: '1'},
    {list_id: '11', product_id: '4', number: '1'},
    
  ]
)
listmember = Listmember.create(
  [
    {list_id: '10', user_id: '2'},
    {list_id: '11', user_id: '4'}
  ]
)
