Rails.application.routes.draw do
  
  get 'products_upc/:upc2' => 'products#semantics'
  #get 'stores/:upc' => 'products#storeRetrieve'
  
  get 'stores/:upc' => 'products#upcitemdbStoreRequest'
  
  get 'list_add_item/:upc/:id' => 'lists#addToList'
  get 'list_add_item_with_list_id/:upc/:list_id' => 'lists#addToListWithListId'
  get 'create_new_list/:name/:user_id' => 'lists#createNewList'
  
  
  
  
  get 'list_change_number/:upc/:id/:number' => 'lists#changeNumber'
  get 'all_lists/:id' => 'lists#getAllLists'
  get 'getSingleListItems/:id' => 'lists#getSingleListItems'
  get 'searchForUserLists' => 'users#searchForUserLists'
  get 'addWishListToFollowing/:user_id/:list_id' => 'lists#addWishListToFollowing'
  
  
  get 'listitems/:id' => 'lists#getListItems'
  get 'listitems/:user_id/:product_id/:number' => 'lists#updateListItemNumber'
  get 'deletelistitem/:user_id/:product_id' => 'lists#deleteListItem'





  get 'check_upc/:upc2' => 'products#getUPCProductName'
  get 'semantics/:upc' => 'products#alwaysUpForSomeAntics'
  get 'upcitemdb/:upc' => 'products#upcitemdbRequest'
  
  
  get 'deleteFromFollowing/:user_id/:list_id' => "lists#deleteFromFollowing"
  get 'addToFollowing/:user_id/:list_id' => "lists#addToFollowing"
  
  
  get 'deleteWishList/:list_id' => "lists#deleteWishList"
  
  
  get 'upcitemdbStoreRequest/:upc' => 'products#upcitemdbStoreRequest'
  
  
  post 'addNewProduct' => 'products#addNewProduct'
  post 'createWishList' => "lists#createWishList"
  
  resources :lists, except: [:new, :edit]
  resources :products, except: [:new, :edit]
  resources :users, except: [:new, :edit]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
