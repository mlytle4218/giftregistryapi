class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  # GET /users
  # GET /users.json
  def index
    @users_new = Array.new

    if params.has_key?(:name) && params.has_key?(:password)
      @users = User.where(name: params[:name]).select(:id, :name, :password)
      if params[:password].eql? @users[0]['password']
        @users_new << {"result" => @users[0]['id'].to_s}
      else
        @users_new << {"result" => "-1"}
      end
      render json: @users_new
    else
      @users = User.select(:id, :name, :last, :password, :email)
      render json: @users
    end

  end

  # GET /users/1
  # GET /users/1.json
  def show
    render json: @user
  end

  # POST /users
  # POST /users.json
  def create
    parsed = JSON.parse(request.raw_post)
    params = ActionController::Parameters.new(parsed)
    params = params.permit(:name, :last, :email, :password, :phone)
    @userCheck = User.where(name: params[:email])
    @user = User.new("name" => parsed["name"], "last" => parsed["last"], "email" => parsed["email"], "password" => parsed["password"], "phone" => parsed["phone"])
    if User.where(email: params[:email]).empty?
      @user.save
      @user_saved = User.where(email: params[:email])
      id = @user_saved.pluck(:id)
      @list = List.new("user_id" => id[0], "name" => parsed['name']+"'s Default List")
      @list.save
      render json: @user, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def searchForUserLists
    @user = User.all
    if params.has_key?(:phone)
      @user = User.where(phone: params[:phone])
    elsif params.has_key?(:email)
      @user = User.where(email: params[:email])
    elsif params.has_key?(:first) && params.has_key?(:last)
      @user = User.where(name: params[:first], last: params[:last])
    end
    if !@user.blank?
      @list = List.where(user_id: @user[0]['id'])
      render json: @list
    else
      render json: {"result"=>"error"}
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    @user = User.find(params[:id])

    if @user.update(user_params)
      head :no_content
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy

    head :no_content
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :phone, :last, :first)
  end

end
