#require 'rubygems'

require 'semantics3'
require 'nokogiri'
require 'open-uri'

API_KEY ="SEM3F4D154FB3E8E6748AB54117D359E2AC4"
API_SECRET="ZmM3OTlhOGEwOTgxZTM5N2RhN2MxOGFmZWIxYTI3OTM"
class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]
  def storeRetrieve
    begin
      page = Nokogiri::HTML(open("http://www.upcitemdb.com/upc/"+ params[:upc]))
      stores=[]
      if !page.css("body > div.container > p.detailtitle").text.strip.include? "has no record"

        page.css("body > div.container > div.tabcon-compare > table > tbody > tr").each do |el|
          store={}
          if !el.css('td:nth-child(1)').text.empty?
            if !el.css('td:nth-child(3)').text.strip.eql? "N/A"
              if el.css('td:nth-child(3)').text.strip[0,1]=="$"
                store["storeName"]= el.css('td:nth-child(1)').text.strip
                store["storeItemName"]= el.css('td:nth-child(2)').text.strip
                store["storeItemUrl"]= "http://www.upcitemdb.com" + el.css('td a').attr('href')
                store["storePrice"]= el.css('td:nth-child(3)').text.strip
              stores.push(store)
              end
            end

          end
        end
        render json: stores
      else
        render json: {"error"=>"result not found"}
      end
    rescue OpenURI::HTTPError => e
      if e.message == '404 Not Found'
        render json: {"error"=>"result not found"}
      end
    end
  end

  def semantics
    @product = Product.where(upc: params[:upc2])
    puts @product.to_json
    if @product.blank?
      puts "in semantics"
      puts params[:upc2]
      begin
        page = Nokogiri::HTML(open("http://www.upcitemdb.com/upc/"+ params[:upc2]))

        thisResults = Hash.new
        detail={}

        if !page.css("body > div.container > p.detailtitle").text.strip.include? "has no record"

          detail["name"] = page.css("body > div.container > p.detailtitle > b").text
          detail["image"]= page.css("body > div.container > div.upcdetail.row > div.l.col-sm-6.col-xs-12 > a > img").attr('src').text
          page.css("body > div.container > div.tabcon-details.row > div.content-box-content2.col-sm-6.col-xs-12 > dl > dd > table > tbody>tr").each do |el|
            thisResults[el.text] =""
            if !el.css('th').text.empty?
              detail[el.css('th').text]=el.css('td').text
            end
          end
          stores=[]

          page.css("body > div.container > div.tabcon-compare > table > tbody > tr").each do |el|
            store={}
            if !el.css('td:nth-child(1)').text.empty?
              if !el.css('td:nth-child(3)').text.strip.eql? "N/A"
                if el.css('td:nth-child(3)').text.strip[0,1]=="$"
                  store["storeName"]= el.css('td:nth-child(1)').text.strip
                  store["storeItemName"]= el.css('td:nth-child(2)').text.strip
                  store["storePrice"]= el.css('td:nth-child(3)').text.strip
                stores.push(store)
                end
              end

            end

          end
          detail["stores"]=stores
        else
          detail = {"error"=>"result not found"}
        end
      rescue OpenURI::HTTPError => e
        if e.message == '404 Not Found'
          detail = {"result"=>"result not found"}
        else
          raise e
        end

      end

      #puts detail["name"]
      #nameFromUPC = getUPCProductName
      #puts nameFromUPC["name"]
      #thisResult = semantics3Search(nameFromUPC["name"])
      #thisResult = semantics3Search("iphone")
      #puts getSampleProdHash
      #thisResult = conglomerateResultHashes(JSON.parse(getSampleProdHash))
      #thisResult = Hash.new
      #thisResult["hi"] = "oops"
      #puts Product.where(upc: params[:upc2]).blank?
      #if Product.where(upc: params[:upc2]).blank?
      #  @product = Product.new
      #  @product.name = detail["name"]
      #  @product.image = detail["image"]
      #  @product.upc = params[:upc2]
      #  @product.description = "Description of product unavailable at this time."

      #@product.save
      render json: detail
    else
      render json: @product
    end

  #render json: nameFromUPC
  end

  def upcitemdbRequest
    require 'net/http'
    result = Net::HTTP.get(URI.parse('https://api.upcitemdb.com/prod/trial/lookup?upc='+params[:upc]))
    parsed = JSON.parse(result)
    @product = Product.where(upc: params[:upc])
    description = 'Description of product unavailable at this time.'
    if @product[0]['description'].eql?'Description of product unavailable at this time.' and !parsed['items'][0]['description'].blank?
      description = parsed['items'][0]['description']
    end
    Product.update(@product[0]['id'], :description => description.slice(0,254),
    :image => parsed['items'][0]['images'][0],
    :ean => parsed['items'][0]['ean'],
    :brand => parsed['items'][0]['brand'],
    :model => parsed['items'][0]['model'],
    :dimension => parsed['items'][0]['dimension'],
    :weight => parsed['items'][0]['weight'])
    if parsed['code'] == 'OK'
      render json: result
    else
      render json: {"error"=>"result not found"}
    end

  end

  def upcitemdbStoreRequest
    require 'net/http'
    result = Net::HTTP.get(URI.parse('https://api.upcitemdb.com/prod/trial/lookup?upc='+params[:upc]))
    parsed = JSON.parse(result)    
    stores=[]
    parsed['items'][0]['offers'].each do |trial|
      store={}
      if !trial['merchant'].blank? && !trial['price'].blank? && !trial['link'].blank? 
        store["storeName"] = trial['merchant']
        store["storeItemUrl"] = trial['link']
        store["storeItemName"] = trial['title']
        store["storePrice"] = trial['price']
        stores.push(store)
      end
    end
    render json: stores
  end

  def alwaysUpForSomeAntics
    sem3 = Semantics3::Products.new(API_KEY, API_SECRET)
    sem3.products_field( "upc", "848447001156" )
    #sem3.products_field( "search", "Samsung Galaxy" )
    prodHash = sem3.get_products()

    render json: prodHash
  end

  def semantics3Search(name)
    sem3 = Semantics3::Products.new(API_KEY, API_SECRET)
    sem3.products_field( "search", name )
    prodHash = sem3.get_products()
    #puts prodHash
    resultHash = Hash.new
    resultHash["results"] = ""
    token = 0
    while ((prodHash = sem3.iterate_products()) && token < 4) do
      puts "token:" + token.to_s
      if !prodHash["results"].nil?
        puts prodHash
      #resultHash["results"] << conglomerateResultHashes(prodHash)
      end
      token += 1
    end

    puts "resultHash.to_json " +resultHash.to_s
    resultHash
  end

  def conglomerateResultHashes(prodHash)
    tempHash = Hash.new
    tempHash["results"]=""
    #puts prodHash["results"].nil?

    prodHash["results"].each do |value|
    # puts "value.blank?" + value.blank?.to_s
    #puts "value.has_key?('sitedetails') " + value.has_key?("sitedetails").to_s
    #puts "value['sitedetails'][0].nil?" + value["sitedetails"][0].nil?.to_s
      if !value.blank? && value.has_key?("sitedetails") && !value["sitedetails"][0].nil? && !value["sitedetails"][0]["latestoffers"][0].nil?
        singleTempHash = Hash.new
        if value["sitedetails"][0]["recentoffers_count"]>0 && !value["upc"].nil?
          singleTempHash["seller"] = value["sitedetails"][0]["latestoffers"][0]["seller"]
          singleTempHash["price"] = value["sitedetails"][0]["latestoffers"][0]["price"]
          singleTempHash["lastrecorded_at"] = value["sitedetails"][0]["latestoffers"][0]["lastrecorded_at"]
          if !value["upc"].nil?
            singleTempHash["upc"] = value["upc"]
          else
            singleTempHash["upc"] = ""
          end
        end
        ##puts "singleTempHash before merge " + singleTempHash.to_json
        ##puts "tempHash before merge " + tempHash.to_json
        tempHash["results"] << singleTempHash.to_json if !singleTempHash.blank?
      ###puts "tempHash['results'] " + tempHash["results"]
      end
    end

    ##puts tempHash
    tempHash["results"]
  end

  # GET /products
  # GET /products.json
  def index
    # if the parameters sent has upc then the user is looking for a specific upc
    # if upc not a parameter it returns all from products
    # if upc is found, name, description and upc number will be returned
    # if upc is not found "{"errors":[{"code":4023,"message":"UPC not found"}]}" is returned
    # if upc is missing from request "{"errors":[{"code":4008,"message":"Missing itemId"}]}" is returned
    if params.has_key?(:upc)
      # Tests to see if upc is in local database
      if !Product.where(upc: params[:upc]).empty?
        #puts "upc found"
        @products = Product.where(upc: params[:upc])
        render json: @products
      else
      # if not found locally, goes to outside source to find upc information
      #puts "upc not found"
        product_holder= Hash.new
        if (product_holder= (check_upcdatabase_org(params[:upc]))).blank?
          puts "upcdatabase.org did not find upc"
          if (product_holder = (check_walmart(params[:upc]))).blank?
            puts "walmart did not find upc"
          end
        end
        ##puts product_holder.blank?
        product_shopping = Hash.new
        if !product_holder.nil? && !product_holder["name"].nil?
          sem3 = Semantics3::Products.new(API_KEY, API_SECRET)
          sem3.products_field( "search", product_holder["name"] )
          #product_shopping = sem3.get_products()
          product_shopping = Hash.new
        product_shopping = sem3.get_products()
        end
        ##puts product_shopping
        #@product_new = Product.new(product_holder)
        @product_new = Product.new()

        if !product_holder.blank?
          if @product_new.save
            #render json: @product_new, status: :created
            #puts product_shopping["results"]
            render json: product_shopping.to_json, status: :created
          else
            render json: @product_new.errors, status: :unprocessable_entity
          end
        else
          render json: {"error"=>"result not found"}
        end
      end
    else
      @products = Product.all
      render json: @products
    end

  end

  def getUPCProductName
    if params.has_key?(:upc2)
      @products = Product.where(upc: params[:upc2])
      if !@products.empty?
        #puts "upc found in local database"
        else
        #puts "upc not found"
        product_holder= Hash.new
        if (product_holder= (check_upcdatabase_org(params[:upc2]))).blank?
          #puts "upcdatabase.org did not find upc"
          if (product_holder = (check_walmart(params[:upc2]))).blank?
            #puts "walmart did not find upc"
            product_holder = {"error"=>"result not found"}
          end
        end
      end
    end
    product_holder
  end

  def check_walmart(upc)
    require 'net/http'
    result = Net::HTTP.get(URI.parse('http://api.walmartlabs.com/v1/items?apiKey=axe67m2j5dg3ft7pt4etsmvm&upc='+upc))
    parsed = JSON.parse(result)
    if parsed.has_key?("errors")
      puts "walmart returned errors"
      nil
    else
      result = Hash.new
      result = {"name"=> parsed["items"][0]["name"], "description" => parsed["items"][0]["shortDescription"], "upc"=>parsed["items"][0]["upc"]}
      fixHashResults(result)
    end
  end

  def check_upcdatabase_org(upc)
    require 'net/http'
    result = Net::HTTP.get(URI.parse('http://api.upcdatabase.org/json/ee6448f8625a815400cf12628c2160db/'+upc))
    parsed = JSON.parse(result)
    if (parsed["valid"] == "false")
      puts "!upcdatabase.org returned a false validity - no product"
      return nil
    else
      result = Hash.new
      result = {"name"=> parsed["itemname"], "description" => parsed["description"], "upc"=>parsed["number"]}
      fixHashResults(result)
    end
  end

  def fixHashResults(result)
    result.each do |key, value|
      holding_string = value.to_s

      if holding_string.include? "&lt;p&gt"; holding_string["&lt;p&gt;"]=""; end

      if holding_string.include? "&lt;/p&gt"; holding_string["&lt;/p&gt;"]=""; end

      if holding_string.length > 251; holding_string = holding_string[0..251]+"..."; end
      result[key]=holding_string
    end
    result
  end

  # GET /products/1
  # GET /products/1.json
  def show
    render json: @product
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # POST /AddNEwProduct
  def addNewProduct
    parsed = JSON.parse(request.raw_post)
    params = ActionController::Parameters.new(parsed)
    params = params.permit(:name, :description,:upc, :brand, :photo)
    @product = Product.new("name" => parsed['name'], "description" => parsed["description"], "upc" => parsed["upc"], "brand" => parsed["brand"], "pic" => parsed["photo]"], "ean" => ("0"+parsed["upc"]))
    if (!parsed['photo'].empty?)
      decodedJson = Base64.decode64(parsed["photo"])
      photo_decoded = StringIO.new(decodedJson)
      photo_decoded.class.class_eval { attr_accessor :original_filename, :content_type }
      photo_decoded.original_filename = "upload.jpg"
      photo_decoded.content_type = "image/jpg"
    @product.pic = photo_decoded
    end

    if @product.save
      render json: @product, status: :created, location: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    @product = Product.find(params[:id])

    if @product.update(product_params)
      head :no_content
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy

    head :no_content
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :upc, :description, :upc2, :photo)
  end

  def getSignatureKey key, dateStamp, regionName, serviceName
    require = 'openssl/hmac'
    kDate    = OpenSSL::HMAC.digest('sha256', "AWS4" + key, dateStamp)
    kRegion  = OpenSSL::HMAC.digest('sha256', kDate, regionName)
    kService = OpenSSL::HMAC.digest('sha256', kRegion, serviceName)
    kSigning = OpenSSL::HMAC.digest('sha256', kService, "aws4_request")

    kSigning
  end

  def getSampleProdHash
    json ="[{
    'merchant': 'Amazon Marketplace Used',
    'domain': 'amazon.com',
    'title': 'Garmin Virb XE',
    'currency': '',
    'list_price': '',
    'price': 283.29,
    'shipping': 'Free Shipping',
    'condition': 'Used',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2t253v2230364d4z2&tid=1&seq=1459953179&plt=d98598dce55db4a4f2c15b8d89bd7d7b',
    'updated_t': 1459951407
  },
  {
    'merchant': 'Amazon Marketplace New',
    'domain': 'amazon.com',
    'title': 'Garmin Virb XE',
    'currency': '',
    'list_price': '',
    'price': 300,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2t2y2u24363b484z2&tid=1&seq=1459953179&plt=7d3e9011f87d3618742a4c96400d64d9',
    'updated_t': 1459494990
  },
  {
    'merchant': 'pcRUSH.com',
    'domain': 'pcrush.com',
    'title': 'GARMIN VIRB XE CAMERA',
    'currency': '',
    'list_price': 399.99,
    'price': 332.99,
    'shipping': '11.60',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s2x2v20343a484w2&tid=1&seq=1459953179&plt=cca73e863631e82825c16468c500fbea',
    'updated_t': 1441381172
  },
  {
    'merchant': 'Amazon.com',
    'domain': 'amazon.com',
    'title': 'Garmin Virb XE',
    'currency': '',
    'list_price': '',
    'price': 333.31,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2t2y2u24363b484y2&tid=1&seq=1459953179&plt=efc29993289b6082f9b212407f6e549f',
    'updated_t': 1459951407
  },
  {
    'merchant': 'Rakuten(Buy.com)',
    'domain': 'rakuten.com',
    'title': 'Garmin VIRB XE HD Action Camera',
    'currency': '',
    'list_price': '',
    'price': 345.28,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2r243x20363f464v2&tid=1&seq=1459953179&plt=c16ea762df11ff89c4460c3ae857a894',
    'updated_t': 1459897971
  },
  {
    'merchant': 'Best Buy',
    'domain': 'bestbuy.com',
    'title': 'Garmin - Virb Xe Hd Action Camera - Black',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s2x2v2w2y294d4v2&tid=1&seq=1459953179&plt=cd62f62dfb0d86e0bb86c391070f5133',
    'updated_t': 1450650946
  },
  {
    'merchant': 'Adorama',
    'domain': 'adorama.com',
    'title': 'Garmin VIRB XE action camera with G-Metrix 1080p/60fps, 240 fps HD slow motion and 12MP photos at up to 30 fps.',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': 'Out of Stock',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2q223z21313c444w2&tid=1&seq=1459953179&plt=5ccaee29caad03edf088f72928f20441',
    'updated_t': 1459940542
  },
  {
    'merchant': 'Factory Outlet Store',
    'domain': 'factoryoutletstore.com',
    'title': 'Garmin VIRB XE HD Action Camera',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '0',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2r2x2x24353b484v2&tid=1&seq=1459953179&plt=909386062bbd1800549b0f6f4582566a',
    'updated_t': 1459582284
  },
  {
    'merchant': 'TackleDirect',
    'domain': 'tackledirect.com',
    'title': 'Garmin VIRB XE Action Camera',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s2z2z2z2x28484y2&tid=1&seq=1459953179&plt=a4c8dfa450d07271200fdc528aae46d8',
    'updated_t': 1459843482
  },
  {
    'merchant': 'TriVillage',
    'domain': 'trivillage.com',
    'title': 'Garmin VIRB XE HD Action Camera Color Dark',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2r2z2z2036394c4z2&tid=1&seq=1459953179&plt=eb3702f9b78e37f14823b4561e9aa639',
    'updated_t': 1459904061
  },
  {
    'merchant': 'Clever Training',
    'domain': 'clevertraining.com',
    'title': 'Garmin VIRB Xe',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2r233u2y2038494v2&tid=1&seq=1459953179&plt=68aaed8a293a5cc50b23e1eea8cd5a5a',
    'updated_t': 1459845447
  },
  {
    'merchant': 'Wal-Mart.com',
    'domain': 'walmart.com',
    'title': 'Garmin VIRB XE HD Action Camera',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s2y2z2w213f454v2&tid=1&seq=1459953179&plt=b3ec8598511cda3b75909cbcde3a9461',
    'updated_t': 1446938217
  },
  {
    'merchant': 'TigerDirect',
    'domain': 'tigerdirect.com',
    'title': 'Garmin VIRB XE - action camera - storage: flash',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '7.28',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s21313z263a484z2&tid=1&seq=1459953179&plt=e35d28618bb8776b2fdf0d0a4ff0e12c',
    'updated_t': 1448323568
  },
  {
    'merchant': 'Bass Pro Shops',
    'domain': 'basspro.com',
    'title': 'Garmin VIRB XE Waterproof HD Action Camera with G-Metrix - Black',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s23323v223a464x2&tid=1&seq=1459953179&plt=9d990141d669cf50224419041b78e496',
    'updated_t': 1459940792
  },
  {
    'merchant': 'West Marine',
    'domain': 'westmarine.com',
    'title': 'Garmin VIRB XE Action Camera',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': 'Out of Stock',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s253v2z243d4a4r2&tid=1&seq=1459953179&plt=516578cccc99224765eed908fa88a5d7',
    'updated_t': 1459950409
  },
  {
    'merchant': 'Dell.com',
    'domain': 'dell.com',
    'title': 'Garmin Virb XE - action camera - storage: flash card',
    'currency': '',
    'list_price': '',
    'price': 399.99,
    'shipping': 'Free Shipping',
    'condition': 'New',
    'availability': 'Out of Stock',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2u2y2x243639464r2&tid=1&seq=1459953179&plt=e64e845f5e2ef0f23b1763eb665beb55',
    'updated_t': 1459775889
  },
  {
    'merchant': 'UnbeatableSale.com',
    'domain': 'unbeatablesale.com',
    'title': 'Garmin USA 010-01363-11 VIRB XE Action Camera',
    'currency': '',
    'list_price': '',
    'price': 441.23,
    'shipping': '9.21',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2t22323332364a4t2&tid=1&seq=1459953179&plt=5fa722d3ed839b4014ee30c79161241a',
    'updated_t': 1459933565
  },
  {
    'merchant': 'TigerDirect Canada',
    'domain': 'tigerdirect.ca',
    'title': 'Garmin VIRB XE - action camera - storage: flash',
    'currency': '',
    'list_price': '',
    'price': 550.99,
    'shipping': '',
    'condition': 'New',
    'availability': '',
    'link': 'http://www.upcitemdb.com/norob/alink/?id=u2s253y2v25394a4t2&tid=1&seq=1459953179&plt=b69fb5f879c02f5400c625d3b5e6528c',
    'updated_t': 1452829706
  }
]"
    json
  end
end
