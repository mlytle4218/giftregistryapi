class ListsController < ApplicationController
  before_action :set_list, only: [:show, :update, :destroy]
  def addToList
    if !List.where(user_id: params[:id]).blank? && !Product.where(upc: params[:upc]).blank?
      @product = Product.where(upc: params[:upc])

      @list = List.where(user_id: params[:id])
      if Listitem.where(list_id: @list[0]['id'], product_id: @product[0]['id'] ).blank?
        @listitems = Listitem.new
        @listitems.list_id = @list[0]['id']
        @listitems.product_id = @product[0]['id']
        @listitems.number = '1'
        @listitems.save
        render json: @listitems
      else
        @listitem =Listitem.where(list_id: @list[0]['id'], product_id: @product[0]['id'] )
        Listitem.update(@listitem[0]['id'], :number => @listitem[0]['number']+1)
        @listitem =Listitem.where(list_id: @list[0]['id'], product_id: @product[0]['id'] )
        render json: @listitem
      end
    else
      render json: {"result"=>"error"}
    end
  end

  def addToListWithListId
    if !Product.where(upc: params[:upc]).blank?
      @product = Product.where(upc: params[:upc])

      @list = List.where(id: params[:list_id])
      puts @list.to_s
      if Listitem.where(list_id: params[:list_id], product_id: @product[0]['id'] ).blank?
        puts "listitem was blank"
        @listitems = Listitem.new
        @listitems.list_id = params[:list_id]
        @listitems.product_id = @product[0]['id']
        @listitems.number = '1'
        @listitems.save
        puts @listitem.to_json
        render json: @listitems
      else
        puts "list item was not blank"
        @listitem =Listitem.where(list_id: params[:list_id], product_id: @product[0]['id'] )
        Listitem.update(@listitem[0]['id'], :number => @listitem[0]['number']+1)
        @listitem =Listitem.where(list_id: params[:list_id], product_id: @product[0]['id'] )
        puts @listitem.to_json
        render json: @listitem
      end
    else
      render json: {"result"=>"error"}
    end
  end

  def changeNumber
    @product = Product.where(upc: params[:upc])
    @list = List.where(user_id: params[:id])
    @listitem = Listitem.where(list_id: @list[0]['id'], product_id: @product[0]['id'])
    puts params[:number]
    if params[:number].eql?"0"
      puts "destroy"
      Listitem.destroy(@listitem[0]['id'])
    else
      Listitem.update(@listitem[0]['id'], :number => 10)
    end

  end

  def getListItems
    if !List.where(user_id: params[:id]).blank?
      @lists = List.where(user_id: params[:id])
      @listitems = Listitem.where(list_id: @lists[0]['id'])
      results = []
      @listitems.each do |items|
        itemArray={}
        itemArray["number"]=items.number
        @prod = Product.where(id: items.product_id)
        itemArray["id"]=@prod[0]['id']
        itemArray["name"]=@prod[0]['name']
        itemArray["description"]=@prod[0]['description']
        itemArray["image"]=@prod[0]['image']
        itemArray["upc"]=@prod[0]['upc']
        results.push(itemArray)
      end
      render json: results
    else
      render json: {"result"=>"error"}
    end

  end

  def getSingleListItems
    @listitems = Listitem.where(list_id: params[:id])
    results = []
    @listitems.each do |items|
      itemArray={}
      itemArray["number"]=items.number
      @prod = Product.where(id: items.product_id)
      itemArray["id"]=@prod[0]['id']
      itemArray["name"]=@prod[0]['name']
      itemArray["description"]=@prod[0]['description']
      itemArray["image"]=@prod[0]['image']
      itemArray["upc"]=@prod[0]['upc']
      results.push(itemArray)
    end
    if (@listitems.blank?)
      render json: {"result" => "error"}
    else
      render json: results
    end
  end

  def updateListItemNumber
    @list = List.where(user_id: params[:user_id])
    @listitem = Listitem.where(list_id: @list[0]["id"], product_id:  params[:product_id])
    if !@listitem.blank?
      if @listitem.update(@listitem[0]["id"], :number => params[:number])
        render json: @listitem
      else
        render json: {"result"=>"error"}
      end
    else
      render json: {"result"=>"error"}
    end
  end

  def deleteListItem
    @list = List.where(user_id: params[:user_id])
    @listitem = Listitem.where(list_id: @list[0]["id"], product_id:  params[:product_id])
    if @listitem.blank?
      render json: {"result"=>"false"}
    else
      if Listitem.destroy(@listitem[0]["id"])
        render json: {"result"=>"true"}
        puts "true"
      else
        render json: {"result"=>"false"}
        puts "false"
      end
    end
  end

  def getAllLists
    results ={}
    lists=[]
    @list = List.where(user_id: params[:id])
    @list.each do |l|
      lists.push(l)
    end
    results['userLists']=lists
    @following = Following.where(users_id: params[:id])
    following=[]
    @following.each do |f|
      @list = List.where(id: f['lists_id'])
      @list.each do |fl|
        following.push(fl)
      end

    end
    results['following']=following
    if (@list.blank? && @following.blank?)
      render json: {"result"=>"error"}
    else
      render json: results
    end

  end

  def addWishListToFollowing
    @following = Following.new(users_id: params[:user_id], lists_id: params[:list_id])
    if @following.save
      render json: @following, status: :created, Location: @following
    else
      render json: @following.errors, status: :unprocessable_entity
    end
  end

  def createNewList
    if params.has_key?(:name) && params.has_key?(:user_id)
      @list = List.new(name: params[:name], user_id: params[:user_id])

      if @list.save
        render json: @list, status: :created, location: @list
      else
        render json: @list.errors, status: :unprocessable_entity
      end
    end
  end

def addToFollowing
    if params.has_key?(:user_id) && params.has_key?(:list_id)
      @check = Following.where(users_id: params[:user_id], lists_id: params[:list_id])
      @following = Following.new(users_id: params[:user_id], lists_id: params[:list_id])
      if @check.blank? && @following.save
        render json: {"result" => "true"}
      else
        render json: {"result" => "false"}
      end
    end
  end

  def deleteFromFollowing
    if params.has_key?(:user_id) && params.has_key?(:list_id)
      @following = Following.where(users_id: params[:user_id], lists_id: params[:list_id])
      puts @following.length
      if !@following.blank?
        if Following.destroy(@following[0]["id"])
          render json: {"result" => "true"}
        else
          render json: {"result" => "false"}
        end
      end
    end
  end

  
  
  def createWishList
    parsed = JSON.parse(request.raw_post)
    params = ActionController::Parameters.new(parsed)
    @check = List.where("name" => parsed["name"], "user_id" => parsed["user_id"])
    @list = List.new("name" => parsed["name"], "user_id" => parsed["user_id"])
    if @check.blank? && @list.save!
      puts "got here"
      puts @list.to_json
      render json: @list#, status: :created, location: @list
        #render json: @list
      else
        render json: {"id" => "-1"}
      end
  end
  
  def deleteWishList
    if params.has_key?(:list_id)
      if List.destroy(params[:list_id])
        render json: {"result" => "true"}
      else
        render json: {"result" => "false"}
      end
    end
  end

  # GET /lists
  # GET /lists.json
  def index
    @lists = List.all

    render json: @lists
  end

  # GET /lists/1
  # GET /lists/1.json
  def show
    render json: @list
  end

  # POST /lists
  # POST /lists.json
  def create
    @list = List.new(list_params)

    if @list.save
      render json: @list, status: :created, location: @list
    else
      render json: @list.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /lists/1
  # PATCH/PUT /lists/1.json
  def update
    @list = List.find(params[:id])

    if @list.update(list_params)
      head :no_content
    else
      render json: @list.errors, status: :unprocessable_entity
    end
  end

  # DELETE /lists/1
  # DELETE /lists/1.json
  def destroy
    @list.destroy

    head :no_content
  end

  private

  def set_list
    @list = List.find(params[:id])
  end

  def list_params
    params.require(:list).permit(:user_id, :product_id, :number, :list_id, :name)
  end
end
